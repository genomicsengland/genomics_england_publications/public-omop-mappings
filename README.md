# Public OMOP Mappings from Genomics England


## Description
This repository contains mappings of NHS data sets to the Observational Medical Outcomes Partnership (OMOP) common data model version 5.4.  Each mapping is supplied in two formats - a human-readable PDF and a machine-readable tab-separated variable (TSV) file. The PDFs should be fairly intuitive, and documentation is supplied for the machine-readable files.

Where applicable, fact relationships have been defined. Most of these are "Has subject relationship context (SNOMED)" / "Subject relationship context of (SNOMED)" but there are some instances where more accurate representations of the relationships are possible, such as "Observation to Measurement" / "Measurement to Observation".

We have endeavoured to map to standard concepts wherever possible, using the SNOMED and LOINC vocabularies, but this has not been possible in all cases. Many of the NHS data sets contain enumerated values, which often have very vague meanings, and we have taken care not to add any information to the OMOP mappings that cannot be safely inferred. For this reason, some of the mappings use non-standard concepts and different vocabularies. Additionally, we have occasionally had to map a given source attribute to more than one OMOP concept and/or domain and link them via fact relationships. 

In order to support traceability, we have defined some short identifiers for each data set to be used as source value prefixes. If these are employed, a source value of 'Consultant' in the Diagnostic Imaging data set would appear as "DID: Consultant" in the OMOP source value.

There are some areas where we have had to make inferences regarding defining visits and episodes. Our normal approach has been to assume that the existence of a record in the NHS data means that a visit took place, even though this is not explicitly stated in the data. These are normally given the value "Entire record" as a source attribute name. 

A further complication we have encountered is the degree of separation of the NHS data between the curated data sets we hold and the original electronic health record. We have adopted a default type concept of "EHR" (concept_id 32817) as we do not have the information needed to safely assume a greater level of granularity. This has also meant that we cannot make full use of the OMOP Oncology extension, as we cannot safely define the episode types because of the absence of explicit outcome information.

We would anticipate that users of the secondary NHS data sets will encounter similar issues. Those users who have direct access to original electronic health records are encouraged to review our visit_occurrence and episode mappings to ascertain if their systems carry additional information that allow the mappings to be better defined for their particular case.

We have used some intermediate tables to arrive at these mappings. These are:

The NICIP to SNOMED mapping, available from [NHS England here.](https://www.england.nhs.uk/statistics/wp-content/uploads/sites/2/2022/03/Annex-5-DID-Interventional-procedures-version-1.xlsx) 

A lookup table of ODS codes. We have used the Organisation Name attribute from the ODS data rather than the Display Name.

Our versions of the intermediate tables will be added to this repository. Users wanting to receive ODS updates will need a TRUD account which can be [created here.](https://isd.digital.nhs.uk/trud/user/guest/group/0/home)

Laura Kerr

13th November 2023

## Support
For support issues, please contact laura.kerr@genomicsengland.co.uk in the first instance.

## Roadmap
Releases to this repository will be made on an iterative basis. When complete, it is envisaged that it will contain mappings for the following NHS data sets:

- National Cancer Registration and Analysis Service (NCRAS)
- National Cancer Registration and Analysis Service Diagnostic Imaging
- Intensive Care National Audit & Research Centre (ICNARC)
- HES Accident and Emergency (HES A&E)
- HES Admitted Patient Care (HES APC)
- HES Critical Care (HES CC)
- HES Outpatient (HES OP)
- Emergency Care Dataset (ECDS)
- NHSD Cancer Registry
- Diagnostic Imaging (DID)
- UKHSA Covid swab tests
- Lung Cancer Dataset 2013 (LUCADA 2013)
- Lung Cancer Dataset 2014 (LUCADA 2014)
- Systemic Anti-Cancer Therapy (SACT)
- Radiotherapy Dataset (RTDS)
- ONS Mortality Dataset
- Mental Health Minimum Dataset (MHMDS)
- Mental Health and Learning Disabilities Dataset (MHLDDS)
- Mental Health Services Data Set (MHSDS)


## Contributing
We are keen to collaborate with other OMOP adopters and welcome feedback and suggestions for improvements. Please contact laura.kerr@genomicsengland.co.uk in the first instance.


## Authors and acknowledgment
This has been a collaborative effort from the following Genomics England staff:

Dr Prabhu Arumugam

Abigail Carter

Helen Carter

Dr Georgia Chan

Laura Kerr

Remi Shah


## License
The mappings in this repository are available under the terms of the GNU public licence.

## Project status
Active.
