This directory contains the OMOP mappings for the NHS Emergency Care Data Sets (ECDS). These are initially supplied in PDF format, with machine-readable versions to be added once they have been proven in Genomics England's ETL pipeline.

**CHANGES SINCE PREVIOUS RELEASE**

A number of mappings have been altered to provide wider domain conformance.

As previously, we would welcome feedback and suggestions for improvements. Please contact laura.kerr@genomicsengland.co.uk in the first instance.

These mappings include concepts for enumeration values as well as coded clinical entries. In order to get as close a match as possible to the data dictionary definitions, we have occasionally had to depart from the OMOP rule of only mapping to standard concepts. Although we have mapped to standard concepts wherever possible, we consider it essential not to add information that cannot be safely inferred. We have used fact relationships to link attributes together, and have occasionally had to map a given source attribute to more than one OMOP concept in order to fully represent the information in the source data.

We have intentionally not mapped ODS codes for organisations that do not provide direct patient care. Examples include Clinical Commissioning Groups (CCGs), NHS Trusts where a lower-level ODS code is available, referring organisations where we cannot identify suitable concepts for the referral type, and so on. While it would theoretically be possible to reconstruct the organisation hierarchy using the NAACCR relationships of 'Has parent item' and 'Parent item of', we do not consider this would add value for researchers, especially as the OMOP care_site domain does not support effective from and effective to dates.

The mappings include codes, rather than concept_ids, in order to support readability. The machine-readable mappings will include the concept_ids where these are known in advance.

**INTERMEDIATE FILES**

Almost every mapping in this repository will require the ODS lookup table that can be found in the intermediate files directory. We would recommend that a process be put in place to maintain this as new versions of the ODS data become available. The lookup table uses the Organisation Name, rather than the Display Name from the ODS data. Where 'ODS' appears in the OMOP Vocabulary column, this indicates a need to use this file. ODS is not a standard OMOP vocabulary.

**VOCABULARIES**

In addition to the intermediate files, this set of mappings will require the OMOP standard vocabularies listed below. These are all available from the [OHDSI Athena](https://athena.ohdsi.org/vocabulary/list) site.

- HCPCS
- LOINC
- SNOMED CT
- UCUM
- UK Biobank
- Visit

**KNOWN ISSUES**

None for this release.

Laura Kerr (laura.kerr@genomicsengland.co.uk)
8th January 2025
